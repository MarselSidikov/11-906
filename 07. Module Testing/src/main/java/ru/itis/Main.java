package ru.itis;

import java.util.Arrays;

/**
 * 07.05.2021
 * 07. Module Testing
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersGenerator generator = new NumbersGenerator(new MathUtil());
        System.out.println(Arrays.toString(generator.generate(10, 100)));
    }
}
