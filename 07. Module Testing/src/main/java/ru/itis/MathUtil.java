package ru.itis;

/**
 * 07.05.2021
 * 07. Module Testing
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MathUtil implements Filter {
    public boolean isPrime(int number) {
        if (number == 0 || number == 1) {
            throw new IncorrectNumberException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean isOk(int number) {
        return isPrime(number);
    }
}
