package ru.itis;

import java.util.Random;

/**
 * 07.05.2021
 * 07. Module Testing
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersGenerator {

    private Random random;
    private Filter filter;

    public NumbersGenerator(Filter filter) {
        this.random = new Random();
        this.filter = filter;
    }

    public int[] generate(int count, int higher) {
        int numbers[] = new int[count];

        for (int i = 0; i < count; i++) {
            int number = random.nextInt(higher);
            while ((number == 0 || number == 1) || !filter.isOk(number)) {
                number = random.nextInt(higher);
            }
            numbers[i] = number;
        }

        return numbers;
    }
}
