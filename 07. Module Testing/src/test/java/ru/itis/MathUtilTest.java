package ru.itis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 07.05.2021
 * 07. Module Testing
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class MathUtilTest {

    private MathUtil mathUtil;

    @BeforeEach
    public void initialize() {
        this.mathUtil = new MathUtil();
    }

    @Test
    public void create_new_math_util() {
        new MathUtil();
    }

    @ParameterizedTest(name = "throws exception for number = {0}")
    @ValueSource(ints = {0, 1})
    public void throws_exception(int number) {
        assertThrows(IncorrectNumberException.class, () -> {
            mathUtil.isPrime(number);
        });
    }

    @ParameterizedTest(name = "returns <false> for number = {0}")
    @ValueSource(ints = {12, 169, 121, 100, 50, 10})
    public void correct_on_not_prime_numbers(int number) {
        assertFalse(mathUtil.isPrime(number));
    }

    @ParameterizedTest(name = "returns <true> for number = {0}")
    @ValueSource(ints = {7, 13, 31, 47, 23, 113})
    public void correct_on_prime_numbers(int number) {
        assertTrue(mathUtil.isPrime(number));
    }
}