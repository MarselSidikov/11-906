package ru.itis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * 07.05.2021
 * 07. Module Testing
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
class NumbersGeneratorTest {

    private NumbersGenerator generator;

    @Mock
    private Filter filter;

    @BeforeEach
    void setUp() {
        when(filter.isOk(anyInt())).thenReturn(false);
        when(filter.isOk(2)).thenReturn(true);
        when(filter.isOk(5)).thenReturn(true);
        generator = new NumbersGenerator(filter);
    }

    @ParameterizedTest
    @CsvSource(delimiter = ',', value = {"10,15", "20,30"})
    void generate(int count, int higher) {
        int result[] = generator.generate(count, higher);
        assertEquals(count, result.length);
    }
}