package ru.itis.base;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DataPrinter {
    public void minMaxPrint(Sequence sequence) {
        System.out.println("MAX - " + sequence.max());
        System.out.println("MIN - " + sequence.min());
    }
}
