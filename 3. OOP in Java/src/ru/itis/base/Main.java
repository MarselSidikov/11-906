package ru.itis.base;

public class Main {

    public static void main(String[] args) {
        int elements[] = {4, 2, 1, 6, 3, 2};
//	    SequenceArrayBasedAbstract sequenceImpl = new SequenceArrayBasedAbstract(elements);
//		Sequence sequence = sequenceImpl;

		Sequence sequence = new SequenceSelectionSortImpl(elements);
	    DataPrinter dataPrinter = new DataPrinter();
//	    dataPrinter.minMaxPrint(sequenceImpl);
	    dataPrinter.minMaxPrint(sequence);

	    MathOperations mathOperations = (SequenceArrayBasedAbstract)sequence;
        System.out.println(mathOperations.average());
    }
}
