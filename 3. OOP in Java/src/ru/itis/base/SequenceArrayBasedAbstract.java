package ru.itis.base;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class SequenceArrayBasedAbstract implements Sequence, MathOperations {

    protected int elements[];
    private boolean isSorted;

    public SequenceArrayBasedAbstract(int elements[]) {
        this.elements = new int[elements.length];
        System.arraycopy(elements, 0, this.elements, 0, elements.length);
    }

    @Override
    public double average() {
        double average = 0;
        for (int i = 0; i < elements.length; i++) {
            average += elements[i];
        }
        return average / elements.length;
    }

    @Override
    public int max() {
        if (!isSorted) {
            sort();
            isSorted = true;
        }
        return elements[elements.length - 1];
    }

    @Override
    public int min() {
        if (!isSorted) {
            sort();
            isSorted = true;
        }
        return elements[0];
    }

    public abstract void sort();
}
