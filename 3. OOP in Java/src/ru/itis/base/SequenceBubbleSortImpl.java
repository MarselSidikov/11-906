package ru.itis.base;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SequenceBubbleSortImpl extends SequenceArrayBasedAbstract {

    public SequenceBubbleSortImpl(int elements[]) {
        super(elements);
    }

    @Override
    public void sort() {
        for (int i = elements.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (elements[j] >= elements[j+1]) {
                    int temp = elements[j];
                    elements[j] = elements[j+1];
                    elements[j+1] = temp;
                }
            }
        }
    }
}
