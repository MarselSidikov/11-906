package ru.itis.collections;

import java.util.Collection;
import java.util.Iterator;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(10);
        myArrayList.add(15);
        myArrayList.add(20);
        myArrayList.add(30);

        for (Integer value : myArrayList) {
            System.out.println(value);
        }

//        Iterable<Integer> iterable = myArrayList;
//
//        Iterator<Integer> iterator = iterable.iterator();
//
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }
//
//        iterator = iterable.iterator();
//
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }

    }
}
