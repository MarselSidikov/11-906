package ru.itis.features;

import java.util.Optional;

/**
 * 25.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FakeStorageImpl implements Storage {
    @Override
    public Optional<User> findUserById(Long id) {
        if (id == 1L) {
            User result = new  User(1L, "Марсель", "Сидиков");
            return Optional.of(result);
        } else return Optional.empty();
    }
}
