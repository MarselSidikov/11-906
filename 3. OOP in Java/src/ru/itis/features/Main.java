package ru.itis.features;

import java.util.Optional;

/**
 * 25.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Storage storage = new FakeStorageImpl();

//        User user = storage.findUserById(2L);
//        if (user == null) {
//            System.exit(0);
//        }
     Optional<User> userOptional = storage.findUserById(1L);

//     if (userOptional.isPresent()) {
//         System.out.println(userOptional.get());
//     }

        userOptional.ifPresent(System.out::println);


    }
}
