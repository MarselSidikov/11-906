package ru.itis.features;

import java.util.Optional;

/**
 * 25.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Storage {
    Optional<User> findUserById(Long id);
}
