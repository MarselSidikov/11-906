package ru.itis.functional;

import ru.itis.base.DataPrinter;
import ru.itis.base.Sequence;
import ru.itis.base.SequenceArrayBasedAbstract;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProgramAnon {
    public static void main(String[] args) {
        Sequence mockSequence = new Sequence() {
            @Override
            public int max() {
                return 100;
            }

            @Override
            public int min() {
                return 0;
            }
        };

        DataPrinter dataPrinter = new DataPrinter();
        dataPrinter.minMaxPrint(mockSequence);

        SequenceArrayBasedAbstract mockAbstractSequence = new SequenceArrayBasedAbstract(null) {
            @Override
            public void sort() {
                throw new NotImplementedException();
            }
        };
    }
}
