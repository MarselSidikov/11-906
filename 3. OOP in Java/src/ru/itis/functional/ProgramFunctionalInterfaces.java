package ru.itis.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProgramFunctionalInterfaces {
    public static void main(String[] args) {
        Function<String, Integer> lettersCountFunction = s -> {
            int count = 0;
            for (int i = 0; i < s.length(); i++) {
                if (Character.isLetter(s.charAt(i))) {
                    count++;
                }
            }
            return count;
        };

        Predicate<Integer> oddNumberPredicate = number -> number % 2 == 1;

        Consumer<Integer> printNumber = System.out::println;

        List<String> words = new ArrayList<>();
        words.add("Marsel007");
        words.add("Azatiks000");
        words.add("KamilQwerty-12");

        Stream<String> wordsStream = words.stream();

        Stream<Integer> lettersCountStream = wordsStream.map(lettersCountFunction);

        Stream<Integer> oddNumbersStream = lettersCountStream.filter(oddNumberPredicate);

        oddNumbersStream.forEach(printNumber);

        words.stream().map(s -> {
            int count = 0;
            for (int i = 0; i < s.length(); i++) {
                if (Character.isLetter(s.charAt(i))) {
                    count++;
                }
            }
            return count;
        }).filter(number -> number % 2 == 1)
                .forEach(System.out::println);

    }
}
