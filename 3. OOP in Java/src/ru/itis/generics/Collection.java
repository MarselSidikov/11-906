package ru.itis.generics;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Collection<A> {
    void add(A a);
}
