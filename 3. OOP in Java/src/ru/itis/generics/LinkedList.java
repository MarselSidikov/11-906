package ru.itis.generics;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<C> implements List<C> {

    private Node<C> first;
    private Node<C> last;

    private static class Node<D> {
        D value;
        Node<D> next;
    }

    @Override
    public C get(int index) {
        return null;
    }

    @Override
    public void add(C c) {

    }
}
