package ru.itis.generics;

/**
 * 18.09.2020
 * 3. OOP in Java
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface List<B> extends Collection<B> {
    B get(int index);
}
