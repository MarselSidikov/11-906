package ru.itis.repositories;

import ru.itis.models.FileInfo;

/**
 * 11.11.2020
 * 4. Simple Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesRepository extends CrudRepository<FileInfo> {
}
