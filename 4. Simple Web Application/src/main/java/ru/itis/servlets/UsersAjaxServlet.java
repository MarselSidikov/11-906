package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.models.User;
import ru.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 14.10.2020
 * 04. Html Servlet
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/users")
public class UsersAjaxServlet extends HttpServlet {

    private ObjectMapper objectMapper = new ObjectMapper();

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        usersService = (UsersService) config.getServletContext().getAttribute("usersService")   ;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/html/ajax_example.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // приняли JSON на вход, с помоьщью ObjectMapper-а превратили в User-объект
        User user = objectMapper.readValue(req.getReader(), User.class);
        // сохранили нового пользователя в бд
        usersService.addUser(user);
        // получили всех пользователей из бд
        List<User> users = usersService.getAll();
        // сформировали JSON-строку-ответ
        String usersAsJson = objectMapper.writeValueAsString(users);
        resp.setContentType("application/json");
        // отправили ответ
        resp.getWriter().println(usersAsJson);
    }
}
