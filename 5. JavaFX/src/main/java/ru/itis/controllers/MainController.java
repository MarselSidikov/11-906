package ru.itis.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 27.11.2020
 * 5. JavaFX
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainController implements Initializable {

    @FXML
    private Button helloButton;

    @FXML
    private Label helloLabel;

    @FXML
    private Circle player;

    @FXML
    private AnchorPane pane;

    public EventHandler<KeyEvent> keyEventEventHandler = event -> {
        if (event.getCode() == KeyCode.UP) {
            player.setLayoutY(player.getLayoutY() - 5);
        } else if (event.getCode() == KeyCode.DOWN) {
            player.setLayoutY(player.getLayoutY() + 5);
        } else if (event.getCode() == KeyCode.CONTROL) {
            Circle bullet = new Circle(player.getLayoutX(), player.getLayoutY(), 5, Color.RED);
            pane.getChildren().add(bullet);

            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
                bullet.setLayoutX(bullet.getLayoutX() + 2);
            }));

            timeline.setCycleCount(500);
            timeline.play();
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        helloButton.setOnAction(event -> {
            helloLabel.setText("Привет!");
        });
    }
}
