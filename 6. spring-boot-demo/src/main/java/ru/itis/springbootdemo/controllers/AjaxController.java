package ru.itis.springbootdemo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.springbootdemo.dto.AjaxDto;

/**
 * 16.04.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class AjaxController {

    @PostMapping("/ajax")
    public ResponseEntity<?> logAjax(@RequestBody AjaxDto dto) {
        System.out.println(dto);
        return ResponseEntity.ok().build();
    }
}
