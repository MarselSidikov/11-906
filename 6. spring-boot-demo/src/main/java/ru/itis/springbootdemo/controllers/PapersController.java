package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.dto.PapersPageDto;
import ru.itis.springbootdemo.models.Paper;
import ru.itis.springbootdemo.services.PapersService;

import java.util.List;

/**
 * 26.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class PapersController {

    @Autowired
    private PapersService papersService;

    @GetMapping("/papers")
    public ResponseEntity<PapersPageDto> getPapers(@RequestParam(value = "sort", required = false) String sort,
                                                   @RequestParam(value = "asc", required = false) Boolean asc,
                                                   @RequestParam("page") Integer page,
                                                   @RequestParam("count") Integer count) {
        return ResponseEntity.ok(papersService.getPapers(sort, asc, page, count));
    }

    @GetMapping("/papers/{paper-id}/like")
    public ResponseEntity<?> like(@RequestParam("account") Long accountId,
                                  @PathVariable("paper-id") Long paperId) {
        papersService.like(accountId, paperId);
        return ResponseEntity.ok().build();
    }
}
