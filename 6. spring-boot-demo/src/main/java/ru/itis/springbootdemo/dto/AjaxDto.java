package ru.itis.springbootdemo.dto;

import lombok.Data;

/**
 * 16.04.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class AjaxDto {
    private String firstName;
    private String lastName;
    private String email;
}
