package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.FileInfo;

/**
 * 12.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByAndStorageFileName(String fileName);
}
