package ru.itis.springbootdemo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Paper;

import java.util.List;

/**
 * 26.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PapersRepository extends JpaRepository<Paper, Long> {
}
