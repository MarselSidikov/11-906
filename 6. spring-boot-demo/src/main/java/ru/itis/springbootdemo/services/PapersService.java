package ru.itis.springbootdemo.services;

import ru.itis.springbootdemo.dto.PapersPageDto;
import ru.itis.springbootdemo.models.Paper;

import java.util.List;

/**
 * 26.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PapersService {
    PapersPageDto getPapers(String sort, Boolean asc, Integer page, Integer count);

    void like(Long accountId, Long paperId);

}
