package ru.itis.springbootdemo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.itis.springbootdemo.dto.PapersPageDto;
import ru.itis.springbootdemo.models.Paper;
import ru.itis.springbootdemo.repositories.PapersRepository;

import java.util.List;

/**
 * 26.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class PapersServiceImpl implements PapersService {
    @Autowired
    private PapersRepository papersRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PapersPageDto getPapers(String sort, Boolean asc, Integer pageNumber, Integer count) {
        Direction direction;
        if (asc != null) {
            direction = Direction.ASC;
        } else {
            direction = Direction.DESC;
        }

        if (sort == null) {
            sort = "id";
        }

        PageRequest pageRequest = PageRequest.of(pageNumber, count, direction, sort);
        Page<Paper> page = papersRepository.findAll(pageRequest);
        return PapersPageDto.builder()
                .pagesCount(page.getTotalPages())
                .papers(page.getContent())
                .build();
    }

    @Override
    public void like(Long accountId, Long paperId) {
        jdbcTemplate.update("insert into user_likes(user_id, paper_id) values (?, ?)", accountId, paperId);
    }
}
