package ru.itis.springbootdemo.services;

import org.springframework.stereotype.Component;

/**
 * 19.03.2021
 * 6. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SmsService {
   void sendSms(String phone, String text);
}
