create table if not exists user_likes (
    user_id bigint,
    paper_id bigint,
    foreign key (user_id) references account(id),
    foreign key (paper_id) references paper(id)
);